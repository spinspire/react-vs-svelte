import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import FileUpload from './FileUpload'

function App() {
  const [files, setFiles] = useState([])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <FileUpload multiple={true} filesCallback={setFiles}>
          <p>React wants you to select some files.</p>
          <ul>
            {files.map(f => <li>{f.name}</li>)}
          </ul>
        </FileUpload>
      </header>
    </div>
  );
}

export default App;
