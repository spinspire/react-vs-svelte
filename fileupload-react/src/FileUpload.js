import React, { useState } from "react"

export default function FileUpload({ multiple = true, filesCallback, children }) {
  const [files, setFiles] = useState([])
  function handleChange(e) {
    setFiles(Array.from(e.target.files))
    filesCallback(Array.from(e.target.files))
  }

  return (
    <>
      <input type="file" multiple={multiple} onChange={handleChange} />
      {children || <p>Select some files.</p>}
      <p>Select some files.</p>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Size</th>
            <th>Type</th>
          </tr>
        </thead>
        <tbody>
          {files.map((f, i) => <tr key={i}>
            <td>{f.name}</td>
            <td>{f.size}</td>
            <td>{f.type}</td>
          </tr>)}
        </tbody>
      </table>
    </>
  )
}