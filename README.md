# ReactJS vs SvelteJS

This is a set of mini projects that show comparison between ReactJS and
SvelteJS approaches to web UI development.

See the screencast [here](https://youtu.be/8dAnBxfR_yQ) for details.
